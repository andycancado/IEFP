﻿using System;

namespace Aula02
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Menu();
        }
        public static void Menu()
        {
            char repetir;
            do
            {
                Console.Clear();
                Console.WriteLine("Insira o primeiro valor:");
                int user0 = int.Parse(Console.ReadLine());
                Console.WriteLine("Insira o Segundo valor:");
                int user1 = int.Parse(Console.ReadLine());
                Console.WriteLine("\n1 - soma \n2 - Subtração \n3 - Multiplicação \n4 - DIvisão \nQualquer outra tecla voltar para trás");
                char final = Console.ReadKey().KeyChar;
                Conta(final, user0, user1);
                Console.WriteLine("Deseja continuar? \n\n1 - para continuar \nQualquer outra tecla para terminar");
                repetir = Console.ReadKey().KeyChar;
            } while (repetir == '1');
        }
        public static void Conta(char z, int x, int y)
        {
            Console.Clear();
            switch (z)
            {
                case '1':
                    int a = Soma(x, y);
                    Console.WriteLine("{0}+{1} = {2}.", x, y, a);
                    break;
                case '2':
                    int b = Subtracao(x, y);
                    Console.WriteLine("{0}-{1} = {2}", x, y, b);
                    break;
                case '3':
                    int c = Multiplicacao(x, y);
                    Console.WriteLine("{0}*{1} = {2}", x, y, c);
                    break;
                case '4':
                    double d = Divisao(x, y);
                    Console.WriteLine("{0}/{1} = {2}.", x, y, d);
                    break;
                default:
                    Console.WriteLine("Numero não valido \nPrima ENTER");
                    Console.ReadKey();
                    Menu();
                    break;
            }
        }
        public static int Soma(int a, int b)
        {
            return a + b;
        }
        public static int Subtracao(int a, int b)
        {
            return a - b;
        }
        public static int Multiplicacao(int a, int b)
        {
            return a * b;
        }
        public static double Divisao(double a, double b)
        {
            return a / b;
        }
    }
}