﻿using System;

namespace Aula03
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            Console.WriteLine ("Insere um numero:");
            long input = long.Parse (Console.ReadLine ());
            Factorial(input);
            Console.ReadKey();
        }
        public static long Factorial (long n)
        {
            if (n == 1) {
                return 1;
            } else {
                long t = n * Factorial(n - 1);
                Console.WriteLine(t);
                return t;
            }
        }
    }
}
/* Aula03 - Recursividade:
 * Um método que aplica a recursividade que invocar-se a si próprio no corpo do método.
*/