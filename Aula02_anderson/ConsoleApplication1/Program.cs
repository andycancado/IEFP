﻿using System;


namespace ConsoleApplication1
{
    class Program
    {
        public static int operacaoAritmetica(int num1, int num2, char op)
        {   //função que realiza as 4 operações aritméticas

            switch (op)
            {
                case '+':
                    {
                        return num1 + num2;
                        break;
                    }

                case '-':
                    {
                        return num1 - num2;
                        break;
                    }
                case '*':
                    {
                        return num1 * num2;
                        break;
                    }
                case '/':
                    {

                        return num1 / num2;
                        break;
                    }
                default:
                    return 0;
                    break;



            }
        }


        public static char Menu()
        {   // função para mostrar o menu e devolve a opção

            Console.WriteLine("1- Soma:");
            Console.WriteLine("2- Subtração:");
            Console.WriteLine("3- Multiplicação:");
            Console.WriteLine("4- Divisão:");
            Console.WriteLine("5- Sair:");
            return Console.ReadKey().KeyChar;

        }
        static void Main(string[] args)
        {
            int escolha;
            int num1 = 0;
            int num2 = 0;
            bool terminar = false;
            do
            {
                //Menu();
                escolha = Menu();
                if (escolha == '5')
                {
                    terminar = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Insira primeiro numero:");
                    num1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Insira segundo numero:");
                    num2 = int.Parse(Console.ReadLine());
                    Console.Clear();
                    switch (escolha)
                    {
                        case '1':
                            {
                                Console.WriteLine("{0} + {1} = {2}\n", num1, num2, operacaoAritmetica(num1, num2, '+'));
                                break;
                            }
                        case '2':
                            {
                                Console.WriteLine("{0} - {1} = {2}\n", num1, num2, operacaoAritmetica(num1, num2, '-'));
                                break;
                            }
                        case '3':
                            {
                                Console.WriteLine("{0} x {1} = {2}\n", num1, num2, operacaoAritmetica(num1, num2, '*'));
                                break;
                            }
                        case '4':
                            {
                                if (num2 == 0)
                                {
                                    Console.WriteLine("divisão não permitida");
                                    break;
                                }
                                Console.WriteLine("{0} / {1} = {2}\n", num1, num2, operacaoAritmetica(num1, num2, '/'));
                                break;
                            }

                        default:
                            {
                                Console.WriteLine(" Opção invalida!!!!");
                                break;
                            }
                    }
                }

            } while (terminar != true);


        }
    }
}
